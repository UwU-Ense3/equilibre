Introduction :

Le but de ce projet est de concevoir un robot pendule inversé sur deux roues capables de se déplacer et de se maintenir en équilibre.

Travail à réaliser :

_Une première étape va consister à concevoir le châssis du robot et de le programmer afin qu'il puisse se maintenir en équilibre.

_Une seconde étape va être de permettre au robot de bouger dans une direction en restant en équilibre.

_Une troisième étape va être de permettre au robot de déplacer tout en évitant les obstacles.

_Une dernière étape va être d'intégrer une connectivité Bluetooth au robot afin qu'il soit contrôler depuis un téléphone ou tout autre module.

