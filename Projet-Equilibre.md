E3 uwu


Projet : Equilibre
============

**Introduction :**

Le but de ce projet est d’utiliser nos connaissances en automatique pour créer un objet motorisé qui garde l’équilibre. Dans un premier temps nous allons utiliser la contre réaction d’une roue pour gérer l’équilibre, et dans un second temps nous allons utiliser cela pour créer un robot à une roue.
D’autre développement sur cette base sont à prévoir et à imaginer.

Si vous êtes intéressé je vous invite à me contacter pour intégrer la conversation Messenger et coordonner nos efforts


**Travail à réaliser :**
   -  Créer un modèle mathématique du pendule.
   -  Linéariser et simuler sur Matlab.
   -  Créer une commande par retour d’état sur Matlab
   -  Créer un observateur et tester avec un capteur d’angle très bruité ou avec une erreur constante.
   -  Vérifier la stabilité de la roue et que la vitesse de rotation ne diverge pas.
   -  Evaluer la dimension et la puissance de la roue nécessaire.
   -  Créer le montage et tester le programme.
   -  Reproduire le processus pour le monocycle.

