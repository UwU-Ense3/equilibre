**Problème de détermination de la position angulaire avec un MPU6050 :**

La détermination de la position angulaire d'un objet est cruciale dans de nombreuses applications, telles que la robotique, la navigation, la stabilisation et la réalité virtuelle. Cependant, les méthodes traditionnelles de détection de la position angulaire en utilisant uniquement l'accéléromètre ou le gyroscope présentent des limitations significatives :

1. **Utilisation de l'accéléromètre uniquement :** L'accéléromètre mesure l'accélération linéaire et permet de calculer l'inclinaison. Cependant, il est sensible aux secousses et aux accélérations linéaires, ce qui peut entraîner une dérive importante dans la mesure de la position angulaire. De plus, l'accéléromètre ne peut pas détecter les mouvements de rotation purs.

2. **Utilisation du gyroscope uniquement :** Le gyroscope mesure la vitesse angulaire, ce qui est idéal pour détecter les mouvements de rotation. Cependant, il est sujet à une dérive à long terme, ce qui signifie que même de petites erreurs dans les mesures s'accumulent au fil du temps. Cela peut rendre la mesure de la position angulaire inutilisable après un certain temps.

**Importance d'un filtre complémentaire :**

Un filtre complémentaire est un outil essentiel pour résoudre les limitations des capteurs individuels, tels que l'accéléromètre et le gyroscope, lors de la mesure de la position angulaire. Le filtre complémentaire combine les avantages de ces deux types de capteurs tout en atténuant leurs inconvénients.

- Il utilise l'accéléromètre pour détecter les inclinaisons à faible fréquence, ce qui permet d'éliminer la dérive à long terme du gyroscope.
- Il utilise le gyroscope pour détecter les mouvements de rotation rapides, réduisant ainsi les erreurs causées par les secousses et les accélérations linéaires.

Le filtre complémentaire combine ces informations de manière à obtenir une mesure plus précise de la position angulaire, offrant une solution robuste et stable pour de nombreuses applications.

Voici un exemple de code Arduino utilisant un filtre complémentaire pour mesurer la position angulaire à l'aide d'un MPU6050 :

```cpp
#include <Wire.h>
#include <MPU6050.h>

MPU6050 mpu;

float accAngle = 0.0; // Angle estimé à partir de l'accéléromètre
float gyroAngle = 0.0; // Angle estimé à partir du gyroscope
float alpha = 0.98; // Coefficient du filtre complémentaire

void setup() {
  Wire.begin();
  Serial.begin(9600);
  mpu.initialize();
}

void loop() {
  int16_t accY = mpu.getAccelerationY();
  int16_t gyroZ = mpu.getRotationZ();

  // Calcul de l'angle à partir de l'accéléromètre
  accAngle = atan2(accY, sqrt(pow(mpu.getAccelerationX(), 2) + pow(mpu.getAccelerationZ(), 2))) * RAD_TO_DEG;

  // Calcul de l'angle à partir du gyroscope
  gyroAngle += gyroZ / 131.0 * 0.01; // 131 LSB/(deg/s), 0.01 secondes par boucle

  // Filtre complémentaire
  float angle = alpha * gyroAngle + (1 - alpha) * accAngle;

  Serial.print("Accel Angle: ");
  Serial.print(accAngle);
  Serial.print(" Gyro Angle: ");
  Serial.print(gyroAngle);
  Serial.print(" Complementary Angle: ");
  Serial.println(angle);
  delay(10);
}
```

Ce code utilise un filtre complémentaire pour combiner les données de l'accéléromètre et du gyroscope afin de mesurer la position angulaire d'un objet de manière plus précise et stable. Le filtre est basé sur une combinaison pondérée des deux mesures, avec le coefficient `alpha` contrôlant l'influence relative de chaque capteur.
