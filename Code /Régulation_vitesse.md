
******************************************************************************************
La boucle interne doit être régulé en vitesse afin de s'assurer que la vitesse désirée pour les roues soient la vitesse effectivement obtenue en sortie.

Afin de réaliser cela, nous allons concevoir un contrôleur de type Proportionnel-Integral-Dérivé (PID). Ce contrôleur doit nous permettre d'asservir la vitesse (suivre la vitesse de référence) assez rapidement (la boucle d'asservissement en vitesse doit être plus rapide que celle en angle).

Malheureusement, nous ne connaissons pas les caractéristiques physique du moteur ( résitance, inductance ...), nous ne pouvons donc pas concevoir une boucle de contrôleur sur matlab afin de simuler le comportement du système, nous allons directement l'implémenter sur Arduino et tuner les gains P,I et D.

******************************************************************************************
Voici ci-dessous un code minimal afin d'asservir en vitesse un moteur DC muni d'un encodeur à quadrature de phase
******************************************************************************************

Le code arduino est le suivant : 

******************************************************************************************

    //Bibliothèque utilisée

    #include <PID_v2.h>  // Pour le PID
    #include <TimerOne.h> // Pour le timer

    //Pins moteur 

    #define EncodeurA 2 // La pin A de l'encodeur relié à la pin D2 qui est une pin d'interruption 
    #define EncodeurB 4 // La pin B de l'arduino reliée à la pin D4 est une pin digitale

    #define ENA A1 // Pin de signal PWM du L298D pour contrôler la vitesse 
    #define IN1 5  // Pin pour le sens de rotation
    #define IN2 6 // Pin pour le sens de rotation

    // volatile => pour toute variable qui sera utilisée dans les interruptions 

    volatile int count1 = 0 ;  // comptage de tick d'encoder qui sera incrémenté sur interruption 

    volatile double speed_measured = 0 ; // Vitesse réelle mésurée

    volatile byte laststate1=0 ; etat précédent de l'encodeur 

    volatile int commande = 0 // Signal de commande qui sera appliquée au moteur
    volatile int speed_ref = 4 ;  // la consigne de vitesse en tr/s
    volatile double Setpoint1, Input1, Output1 ;// Setpoint==le point référence visé. Output1==Ce qui est envoyé aux moteurs.

    volatile double Kp=15, Ki=40, Kd=3;// Constante du PID

    //Initialisation du PID

    PID PID1(&Input1, &Output1, &Setpoint1, Kp, Ki, Kd, DIRECT);

    //Fonction mouvement

    void forward(int vitesse);

    // Fonction setup 

    void setup() {

    // Setup des pins moteur et driver

    pinMode(IN1,OUTPUT);
    pinMode(IN2,OUTPUT);
    pinMode(ENA,OUTPUT);

    //Setup pour les encodeurs

    pinMode(EncodeurA,INPUT_PULLUP);
    pinMode(EncodeurB,INPUT_PULLUP);

    //Serial port initialization

    Serial.begin(9600);  

    //Comptage de l'encodeur 

    attachInterrupt(0,counter1, CHANGE);// l'interruption 0 (sur une pin de l'arduino, la pin digititale 2 intervient sur changement d'état de l'entrée

    Timer1.initialize(100000); // set a timer of length 100000 microseconds //(or 0.1 sec - or 10Hz => the led will blink 5 times, 5 cycles of on-and-off, per second) 
    Timer1.attachInterrupt( timerIsr );// Fonction à appeler lors de l'interruption 

    // PID 

    Input1 = 0;
    Setpoint1 = 0;// Réference souhaitée


    //active le PID
    PID1.SetMode(AUTOMATIC);
    PID1.SetOutputLimits(-255, 255);// Limiter la sortie à -255 min et 255 max

    }

    void loop() 
    { 

        // Serial.println(speed_measured);/// Affichage moniteur/traceur série
        // Serial.print(",");

    
    forward(output1);
    //delay(100);
    }


    void timerIsr()
    {  
        speed_measured = count1*10/100/50; // vitesse en tour/seconde // count => nb de ticks encodeurs, 100=>nb tick //par tour de l'arbre moteur, 10=> frequency of Isr

        // 50 : rapport de réduction
        // 10 : fréquence de timeIsr
        //100 : nb de ticks (haut+bas)
        count1=0;
        Setpoint1 = speed_ref ;
        Input1=speed1;
        PID1.Compute();// Calcul des paramètres du PID
        commande = (int)Output1; // cast vers un entier

    }

    void counter1() 
    {
    byte state1=PIND;
    
    state1|=B11101011;  // mask pour ne regarder que les changements sur 2 et 4 
    // Modifier le MASK  B01111011 par BXXXXXXXX mettre des 0 là où sont les pins utilisés par l'encodeur
    if( state1!=laststate1)
    {
        (((state1&(1<<EncodeurA))>>EncodeurA)^((state1&(1<<EncodeurB))>>EncodeurB))?count1--:count1++;
        laststate1=state1;
    }
    }

    void forward(int vitesse)
    {
    digitalWrite(IN1,HIGH);
    digitalWrite(IN2,LOW);
    analogWrite(ENA,vitesse);
    }

********************************************************************************************************Résumé du code 
********************************************************************************************************

Ce code Arduino a pour objectif de contrôler la vitesse d'un moteur DC équipé d'un encodeur à quadrature de phase en utilisant un régulateur PID (Proportionnel-Intégral-Dérivé). Voici un résumé bref du code et de son fonctionnement :

1. Les bibliothèques `PID_v2` et `TimerOne` sont incluses pour le contrôle PID et la gestion des interruptions de minuterie.

2. Les broches Arduino sont définies pour les encodeurs, le contrôle du moteur et d'autres fins.

3. Des variables volatiles sont utilisées pour stocker des données modifiées dans des interruptions.

4. Un objet PID est initialisé avec des constantes de contrôle P, I et D, ainsi que des variables d'entrée, de sortie et de consigne.

5. La fonction `forward` est utilisée pour contrôler la direction du moteur en définissant les broches IN1 et IN2, et la vitesse du moteur est contrôlée via la broche ENA.

6. Dans la fonction `setup`, les broches sont configurées, les interruptions d'encodeur sont définies, et le PID est activé en mode automatique.

7. La fonction `loop` exécute principalement la fonction `forward` avec la sortie du PID comme vitesse du moteur.

8. La fonction `timerIsr` est une interruption périodique qui calcule la vitesse en utilisant les compteurs incrémentés dans la fonction `counter1`. Elle ajuste également la consigne de vitesse (Setpoint1) et utilise le PID pour déterminer la commande de vitesse (commande).

9. La fonction `counter1` est une interruption déclenchée lors de changements d'état des broches de l'encodeur. Elle compte les impulsions et détermine la direction de rotation.

En résumé, le code utilise un régulateur PID pour suivre une consigne de vitesse pour un moteur DC en se basant sur les informations de l'encodeur. Les interruptions sont utilisées pour compter les impulsions de l'encodeur, et la boucle PID ajuste la commande de vitesse pour maintenir la vitesse réelle aussi proche que possible de la consigne. Le résultat est la commande du moteur qui maintient la vitesse désirée malgré les variations du système.